
public class Shape
{
  private int size;
  private String output = "";

  public Shape(int intSize)
  {
    size = intSize;
  }

  public String toString()
  {
    return output;
  }

  public void shapeA()
  {
    for (int row = size; row > 0; row--)
    {
      for (int star = 1; star <= row; star++)
        output += '*';
      output += '\n';//next row
    }
  }

  public void shapeB()
  {
    for (int row = 1; row <= size; row++)
    {
      for (int star = 1; star <= size; star++)
      {
        if (star > size - row)
          output += '*';
        else
          output += ' ';
      }
      output += '\n';//next row
    }
  }

  public void shapeC()
  {
    for (int row = 0; row < size; row++)
    {
      for (int star = size; star > 0; star--)
      {
        if (star > size - row)
          output += ' ';
        else
          output += '*';
      }
      output += '\n';//next row
    }
  }

  public void shapeD()
  {
    int div = (int) Math.ceil((float) size / 2);//round up integer division
    for (int row = 1; row <= size; row++)
    {
      int invRow = div + (div - row);
      if (row <= size / 2)
      {
        for (int space = 1; space <= (div - row); space++)
          output += ' ';
        for (int star = 1; star <= ((row - 1) * 2 + 1); star++)
          output += '*';
        if (row == div && size % 2 == 0)//hack for even numbers
        {
          output += '\n';//add a row
          for (int star = 1; star <= ((row - 1) * 2 + 1); star++)
            output += '*';
        }
      }
      else
      {
        for (int space = 1; space <= (div - invRow); space++)
          output += ' ';
        for (int star = 1; star <= ((invRow - 1) * 2 + 1); star++)
          output += '*';
      }
      output += '\n';//next row
    }
  }
}
