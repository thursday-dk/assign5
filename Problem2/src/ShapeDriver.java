
//********************************************************************
//File:	        ShapeDriver.java      
//Author:       Kalvin Dewey
//Date:	        10/28/17
//Course:       CPS100
//
//Problem Statement:
//Create modified versions of the Stars program to print the fol-
//lowing patterns. Create a separate program to produce each pat-
//tern. Hint: Parts b, c, and d require several loops, some of which
//print a specific number of spaces.
//
//a.********** b.        * c.********** d.    *    
//  *********           **    *********      ***   
//  ********           ***     ********     *****  
//  *******           ****      *******    ******* 
//  ******           *****       ******   *********
//  *****           ******        *****   *********
//  ****           *******         ****    ******* 
//  ***           ********          ***     *****  
//  **           *********           **      ***   
//  *           **********            *       *
//
//Your solution should satisfy these requirements:
//Allow a user to specify a size which should be used for each shape.
//All four shapes are created in their own methods in a class named Shape.
//It is your Driver class that displays the required shapes.
//********************************************************************
import java.util.Scanner;


public class ShapeDriver
{

  public static void main(String[] args)
  {
    char shapeType = ' ';
    Scanner scan = new Scanner(System.in);
    
    System.out.println("How big is the shape?");
    int shapeSize = scan.nextInt();
    scan.nextLine();//consumes newline
    
    Shape shape = new Shape(shapeSize);
    System.out.println("Select a shape. (a,b,c or d)");
    String shapeLine = scan.nextLine();
    if (shapeLine.length() > 0)
      shapeType = shapeLine.toLowerCase().charAt(0);
    
    switch (shapeType)
    {
    case 'a':
      shape.shapeA();
      break;
    case 'b':
      shape.shapeB();
      break;
    case 'c':
      shape.shapeC();
      break;
    case 'd':
      shape.shapeD();
      break;
    default:
      System.out.println("No shape selected");
    }
    System.out.println(shape);
    scan.close();
  }

}
