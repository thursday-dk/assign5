
//********************************************************************
//File:         MultTables.java      
//Author:       Kalvin Dewey
//Date:         10/29/17
//Course:       CPS100
//
//Problem Statement:
//Write a program that produces a multiplication table,
//showing the results of multiplying the integers
//1 through 12 by  themselves.
//********************************************************************

public class MultTables
{
  public static void main(String[] args)
  {
    final int TABLE_SIZE = 12;
    Multiplier multiplier = new Multiplier(TABLE_SIZE);
    
    System.out.print("*");
    for (int column = 1; column <= TABLE_SIZE; column++)
      System.out.print("\t" + column);
    System.out.println();
    for (int row = 1; row <= TABLE_SIZE; row++)
    {
      System.out.print(row);
      System.out.println(multiplier.MultiplesOf(row));
    }
  }
}
