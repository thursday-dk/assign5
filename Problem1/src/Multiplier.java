
public class Multiplier
{
  private int size = 0;

  public Multiplier(int intSize)
  {
    size = intSize;
  }

  public String MultiplesOf(int value)
  {
    String output = "";
    for (int mult = 1; mult <= size; mult++)
    {
      output += "\t" + value * mult;
    }
    return output;
  }
}
